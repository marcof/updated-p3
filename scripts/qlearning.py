# mdp implementation needs to go here
from read_config import read_config
from copy import deepcopy
import math
import numpy
import random

class QLearn():

    def __init__(self):
        
        self.config = read_config()
        
        self.start = self.config["start"]
        self.goal = self.config["goal"]
        self.move_list = self.config["move_list"]
        self.walls = self.config["walls"]
        self.pits = self.config["pits"]
        self.map_size = self.config["map_size"]

        self.obstacles = self.walls + self.pits
        
        # MDP variables
        self.max_iterations = self.config["max_iterations"]
        self.threshold_difference = self.config["threshold_difference"]

        self.reward_for_each_step = self.config["reward_for_each_step"]
        self.reward_for_hitting_wall = self.config["reward_for_hitting_wall"]
        self.reward_for_reaching_goal = self.config["reward_for_reaching_goal"]
        self.reward_for_falling_in_pit = self.config["reward_for_falling_in_pit"]

        self.discount_factor = self.config["discount_factor"]
        self.gamma = 0.5

        self.prob_move_forward = self.config["prob_move_forward"]
        self.prob_move_backward = self.config["prob_move_backward"]
        self.prob_move_left = self.config["prob_move_left"]
        self.prob_move_right = self.config["prob_move_right"]
        
        self.moves = [[0, 1], [1, 0], [0, -1], [-1, 0]]
        self.moves_dir = ["E", "S", "W", "N"]

        self.prob_move = [self.prob_move_forward, self.prob_move_backward, self.prob_move_right, self.prob_move_left]

        self.rows = self.map_size[0]
        self.columns = self.map_size[1]
        
        self.weights = []
        self.possible_start_positions = []
        for i in range(0, self.rows):
            inter = []
            for j in range(0, self.columns):
                inter.append("")
                self.possible_start_positions.append([i, j])
            self.weights.append(inter)
        
        print self.pits
        print self.walls
        print self.goal
        
        for pit in self.pits:
            self.weights[pit[0]][pit[1]] = "PIT"
            self.possible_start_positions.remove([pit[0], pit[1]])

        for wall in self.walls:
            self.weights[wall[0]][wall[1]] = "WALL"
            self.possible_start_positions.remove([wall[0], wall[1]])

        self.weights[self.goal[0]][self.goal[1]] = "GOAL"
        self.possible_start_positions.remove([self.goal[0], self.goal[1]])
        
        print self.weights

        #self.values = []
        #for i in range(0, self.rows):
        #    inter = []
        #    for j in range(0, self.columns):
        #        inter.append(0.0)
        #    self.values.append(inter)

        self.q_values = []
        for i in range(0, self.rows):
            inter = []
            for j in range(0, self.columns):
                inter.append([0.0, 0.0, 0.0, 0.0])
            self.q_values.append(inter)
        
        self.C = self.config["C"]
        self.iterations = []
        for i in range(0, self.rows):
            inter = []
            for j in range(0, self.columns):
                inter.append([1.0, 1.0, 1.0, 1.0])
            self.iterations.append(inter)

        print self.q_values
        print self.iterations
        print self.possible_start_positions
        
        #self.full_values = []
        #for i in range(0, self.row):
        #    inter = []
        #    for j in range(0, self.columns):
        #        inter.append([0.0, 0.0, 0.0, 0.0])
        #    self.full_values.append(inter)

        #for pit in self.pits:
        #    self.values[pit[0]][pit[1]] = self.reward_for_falling_in_pit

        #for wall in self.walls:
        #    self.values[wall[0]][wall[1]] = self.reward_for_hitting_wall

        #self.values[self.goal[0]][self.goal[1]] = self.reward_for_reaching_goal
            
    def iterate(self):
        print "iterate"
        
        # generate random start position
        start = random.choice(self.possible_start_positions)
        
        #start exploration
        current = start
        for i in range(0, self.rows*self.columns):
           #print "Current spot"
           # print current
            full_map = self.get_full_map(current[0], current[1])
           # print full_map
            #print full_map
            # pick the highest combined value
            place = numpy.argmax(full_map)
            #print place
            move = self.moves[place]

            forward = move
            backward = negate(move)
            left = flip(move)
            right = flip(negate(move))

            possible = [forward, backward, right, left]
            index = [0, 1, 2, 3]
            pick_move = numpy.random.choice(index, p=self.prob_move)

            next_move = possible[pick_move]
            #print "Move"
            #print place
            #print move

            #print "Actual Move"
            #print pick_move
            #print possible
            #print next_move

            V = 0
            spot = merge(current, next_move)
            #print spot
            #update_index = merge(current, forward)
            finish_exp = False
            if check_out(spot, self.rows, self.columns, self.walls):
                prev_val = self.q_values[current[0]][current[1]][place]
                future_q = numpy.max(self.q_values[current[0]][current[1]])
                reward = self.reward_for_hitting_wall
                #print "wall hit"
                spot = current
            else:
                #print self.values
                prev_val = self.q_values[current[0]][current[1]][place]
                future_q = numpy.max(self.q_values[spot[0]][spot[1]])
                reward = self.reward_for_each_step
                if self.weights[spot[0]][spot[1]] == "PIT":
                    reward += self.reward_for_falling_in_pit
                    finish_exp = True
                    #print "pit hit"
                elif self.weights[spot[0]][spot[1]] == "GOAL":
                    reward += self.reward_for_reaching_goal
                    finish_exp = True
                    #print "goal hit"
            #print "Prev_val "+ str(prev_val)
            #print "Reward "+ str(reward)
            #print "Future best q "+str(future_q)
            prob = 1
            value = prob * (reward + self.discount_factor * future_q)
            V += value
            self.q_values[current[0]][current[1]][place] = (1-self.gamma) * prev_val + self.gamma *V
            #print self.iterations[current[0]][current[1]]
            self.iterations[current[0]][current[1]][place] += 1
            current = spot
            #print self.q_values
            #print self.iterations
            #a = raw_input("Press Enter to continue...") 
            if finish_exp:
                break
        #print self.weights
        for i in range(0, self.rows):
            for j in range(0, self.columns):
                #print len(self.weights[i])
                #print j
                #print self.weights[i][j]
                if self.weights[i][j] != "WALL" and self.weights[i][j] != "PIT" and self.weights[i][j] != "GOAL":
                    direction = numpy.argmax(self.q_values[i][j])
                    self.weights[i][j] = self.moves_dir[direction]
        return self.weights

            #print self.q_values
            #print current
            #print

    def get_full_map(self, i, j):
        full = []
        #print self.iterations
        #print self.q_values
        for x in range(0, 4):
            full.append(self.q_values[i][j][x] + float(self.C) / self.iterations[i][j][x])
        return full
def check_out(item, rows, columns, walls):
    row, col = item
    if row < 0 or row >= rows or col < 0 or col >= columns:
        return True

    for wall in walls:
        if row == wall[0] and col == wall[1]:
            return True
    return False


def merge(start, move):
    return [start[0] + move[0], start[1] + move[1]]

def flip(item):
    return [item[1], item[0]]

def negate(item):
    new = [0.0, 0.0]
    new[0] = item[0] * -1
    new[1] = item[1] * -1
    return new

if __name__ == '__main__':
    q = QLearn()
    for i in range(0, 1000):
        weights = q.iterate()
    print q.weights
    print q.q_values
    print weights
