# astar implementation needs to go here
import math
import heapq


class Grid():
    def __init__(self, place, bscore, fscore, parent):
        self.place = place
        self.bscore = bscore
        self.fscore = fscore
        self.parent = parent

    @property
    def score(self):
        return self.bscore + self.fscore
    
    def __str__(self):
        output = ""
        output += str(self.place) + "\n"
        output += str(self.bscore) + "\n"
        output += str(self.fscore) + "\n"
        return output

def run_astar(start, goal, move_list, obstacles, map_size):
    
    frontier = set()
    finished = set()

    mapping = {}

    startNode = Grid(start, 0, dist(start, goal), None)
    mapping[str(start)] = startNode
    frontier.add(str(start))

    while len(frontier) > 0:
        grids = sorted(mapping.values(), key=lambda grid: grid.score)
        grid = None
        for g in grids:
            if str(g.place) not in finished:
                grid = g
                break
       
        if grid.place == goal:
            return get_path(grid)
        
        if str(grid.place) in frontier:
            frontier.remove(str(grid.place))

        finished.add(str(grid.place))

        neighbors = get_neighbors(grid.place, move_list, map_size, obstacles)
        print "Front " + str(grid.place)
        print "Neigh " + str(neighbors)
        for neighbor in neighbors:

            if str(neighbor) in finished:
                continue

            if str(neighbor) in mapping:
                old = mapping[str(neighbor)]
                dist_before = grid.bscore + 1
                if dist_before < old.bscore:
                    old.bscore = dist_before
                    old.parent = grid
            else:
                dist_before = grid.bscore + 1
                dist_after = dist(neighbor, goal)
                new = Grid(neighbor, dist_before, dist_after, grid)
                mapping[str(neighbor)] = new
                frontier.add(str(neighbor))

def get_path(grid):
    output = []

    current = grid
    while(current != None):
        output.append(current.place)
        current = current.parent

    
    return list(reversed(output))

def get_neighbors(point, move_list, map_size, obstacles):
    neighbors = []
    for move in move_list:
        new_point = [point[0] + move[0], point[1]+move[1]]
        if new_point not in obstacles and check_bounds(new_point, map_size):
            neighbors.append(new_point)
    return neighbors

def check_bounds(p, map_size):
    x, y = p
    if x >= 0 and x < map_size[0] and y >= 0 and y < map_size[1]:
        return True
    return False



def dist(p1, p2):
    dist = math.fabs(p1[0] - p2[0]) + math.fabs(p1[1] - p2[1])
    return dist
