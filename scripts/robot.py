#!/usr/bin/env python

#robot.py implementation goes here

import math
import numpy as np
import random

import rospy
from read_config import read_config
from cse_190_assi_3.msg import AStarPath, PolicyList
from std_msgs.msg import Bool
from astar import run_astar
from mdp import MDP
from qlearning import QLearn

class Robot():

    def __init__(self):
        rospy.init_node("robot")
        
        self.config = read_config()
        
        self.start = self.config["start"]
        self.goal = self.config["goal"]
        self.move_list = self.config["move_list"]
        self.walls = self.config["walls"]
        self.pits = self.config["pits"]
        self.map_size = self.config["map_size"]

        self.obstacles = self.walls + self.pits

        self.max_iterations = self.config["max_iterations"]
        self.threshold_difference = self.config["threshold_difference"]
        
        self.results_astar = rospy.Publisher(
            "/results/path_list",
            AStarPath,
            queue_size = 10,
        )
        self.sim_complete = rospy.Publisher(
            "/map_node/sim_complete",
            Bool,
            queue_size = 10
        )
        self.results_mdp = rospy.Publisher(
            "/results/policy_list",
            PolicyList,
            queue_size = 10
        )

        #a_star()
        rospy.sleep(1)
        self.run()


    def a_star(self):
        output = run_astar(self.start, self.goal, self.move_list, self.obstacles, self.map_size)
        for step in output:
            self.results_astar.publish(step)
            rospy.sleep(.01)
        print output

    def mdp(self):

        m = MDP()
        for i in range(0, self.max_iterations): 
            cont, result = m.iterate()
            final = []
            # flatten result
            for row in result:
                final += row
            if not cont:
                break
            self.results_mdp.publish(PolicyList(final))
    def qlearn(self):
        q = QLearn()
        for i in range(0, self.max_iterations): 
            result = q.iterate()
            final = []
            # flatten result
            for row in result:
                final += row
            self.results_mdp.publish(PolicyList(final))
        print final
                
    def run(self):

        # A star
        #self.a_star()

        # MDP
        print "start qlearn"
        rospy.sleep(1)
        #self.mdp()
        self.qlearn()

        self.sim_complete.publish(Bool(True))
        rospy.sleep(1)
        rospy.signal_shutdown("finished")

if __name__ == '__main__':
    r = Robot()
    #r.a_star()
