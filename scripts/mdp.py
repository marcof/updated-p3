# mdp implementation needs to go here
from read_config import read_config
from copy import deepcopy
import math
import numpy

class MDP():

    def __init__(self):
        
        self.config = read_config()
        
        self.start = self.config["start"]
        self.goal = self.config["goal"]
        self.move_list = self.config["move_list"]
        self.walls = self.config["walls"]
        self.pits = self.config["pits"]
        self.map_size = self.config["map_size"]

        self.obstacles = self.walls + self.pits
        
        # MDP variables
        self.max_iterations = self.config["max_iterations"]
        self.threshold_difference = self.config["threshold_difference"]

        self.reward_for_each_step = self.config["reward_for_each_step"]
        self.reward_for_hitting_wall = self.config["reward_for_hitting_wall"]
        self.reward_for_reaching_goal = self.config["reward_for_reaching_goal"]
        self.reward_for_falling_in_pit = self.config["reward_for_falling_in_pit"]

        self.discount_factor = self.config["discount_factor"]

        self.prob_move_forward = self.config["prob_move_forward"]
        self.prob_move_backward = self.config["prob_move_backward"]
        self.prob_move_left = self.config["prob_move_left"]
        self.prob_move_right = self.config["prob_move_right"]
        
        self.moves = [[0, 1], [1, 0], [0, -1], [-1, 0]]
        self.moves_dir = ["E", "S", "W", "N"]

        self.prob_move = [self.prob_move_forward, self.prob_move_backward, self.prob_move_right, self.prob_move_left]

        self.rows = self.map_size[0]
        self.columns = self.map_size[1]
        
        self.weights = []
        for i in range(0, self.rows):
            inter = []
            for j in range(0, self.columns):
                inter.append("")
            self.weights.append(inter)
        
        print self.pits
        print self.walls
        print self.goal
        
        for pit in self.pits:
            self.weights[pit[0]][pit[1]] = "PIT"

        for wall in self.walls:
            self.weights[wall[0]][wall[1]] = "WALL"

        self.weights[self.goal[0]][self.goal[1]] = "GOAL"
        
        print self.weights

        self.values = []
        for i in range(0, self.rows):
            inter = []
            for j in range(0, self.columns):
                inter.append(0.0)
            self.values.append(inter)

        #for pit in self.pits:
        #    self.values[pit[0]][pit[1]] = self.reward_for_falling_in_pit

        #for wall in self.walls:
        #    self.values[wall[0]][wall[1]] = self.reward_for_hitting_wall

        #self.values[self.goal[0]][self.goal[1]] = self.reward_for_reaching_goal
        print self.values
            
    def iterate(self):
        print "iterate"
        new_values = deepcopy(self.values)
        new_weights = deepcopy(self.weights)
       
        # iterate through grid
        for i in range(0, self.rows):
            for j in range(0, self.columns):

                if self.weights[i][j] != "WALL" and self.weights[i][j] != "PIT" and self.weights[i][j] != "GOAL":
                    current = [i, j]

                    output = []
                    #print "possible moves"
                    for move in self.moves:
                        new_spot = merge(current, move)
                        #   print "New spot: " +str(new_spot)
                        #  print "newspot ^"
                        forward = move
                        backward = negate(move)
                        left = flip(move)
                        right = flip(negate(move))

                        possible = [forward, backward, right, left]
                        #print possible
                        
                        V = 0
                        for p in range(0, len(possible)):
                            pos = possible[p]
                            spot = merge(current, pos)
                            #print spot
                            if check_out(spot, self.rows, self.columns, self.walls):
                                prev_val = self.values[current[0]][current[1]]
                                reward = self.reward_for_hitting_wall
                            else:
                                #print self.values
                                prev_val = self.values[spot[0]][spot[1]]
                                reward = self.reward_for_each_step
                                if self.weights[spot[0]][spot[1]] == "PIT":
                                    reward += self.reward_for_falling_in_pit
                                if self.weights[spot[0]][spot[1]] == "GOAL":
                                    reward += self.reward_for_reaching_goal


                            prob = self.prob_move[p]

                            value = prob * (reward + self.discount_factor * prev_val)
                            V += value
                        output.append(V)

                    # find best move
                    #print "output"
                    #print output
                    val = max(output)
                    index = output.index(val)
                    #print index
                    #print "Best Move "+str(self.moves[index])
                    #print "Direction " +str(self.moves_dir[index])
                    #print val
                    #print i, j
                    new_values[i][j] = val
                    new_weights[i][j] = self.moves_dir[index]


        
        cont = True
        diff = numpy.sum(numpy.fabs(numpy.array(new_values) - numpy.array(self.values)))

        if diff < self.threshold_difference:
            cont = False
            print "cont to false we done boise"
        self.weights = new_weights
        self.values = new_values
        return cont, deepcopy(self.weights)

def check_out(item, rows, columns, walls):
    row, col = item
    if row < 0 or row >= rows or col < 0 or col >= columns:
        return True

    for wall in walls:
        if row == wall[0] and col == wall[1]:
            return True
    return False


def merge(start, move):
    return [start[0] + move[0], start[1] + move[1]]

def flip(item):
    return [item[1], item[0]]

def negate(item):
    new = [0.0, 0.0]
    new[0] = item[0] * -1
    new[1] = item[1] * -1
    return new

if __name__ == '__main__':
    m = MDP()
    for i in range(0, 1000):
        cont, result = m.iterate()
        print cont
        if not cont:
            print "stopped on iter " +str(i)
            break
    print result
